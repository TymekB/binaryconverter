var input = document.querySelector("#decimal");
var output = document.querySelector("#binary");
var logo = document.querySelector("#logo");

input.addEventListener('keyup', function()
{
	var inputValue = parseInt(input.value);
	var result = decimalToBinary(inputValue);

	output.value = result;

});

function decimalToBinary(number)
{
	var binaryArray = [];
	var binary = "";
	var result = number;

	while(result > 0)
	{
		binaryArray.push(result % 2);

		result = Math.floor(result / 2);
	}

	binaryArray.reverse();

	for(var i = 0; i < binaryArray.length; i++)
	{
		binary += binaryArray[i];
	}

	return binary;
	
}

var index = 0;

var timer = setInterval(function(){
	typing("Binary Converter");
}, 80);

function typing(text)
{
	if(text.length > index)
	{
		logo.textContent += text[index];
		index++;
	}
	else
	{
		clearInterval(timer);
	}
}